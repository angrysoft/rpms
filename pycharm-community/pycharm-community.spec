%global _missing_build_ids_terminate_build 0

Name:           pycharm-community
Version:        2017.3.2
Release:        1%{?dist}
Summary:        Intelligent Python IDE

License:        ASL 2.0
URL:            http://www.jetbrains.com/pycharm/
Source0:        https://download.jetbrains.com/python/%{name}-%{version}.tar.gz
Source1:        pycharm-community.desktop

BuildRequires: desktop-file-utils
BuildRequires: python3-devel

Requires:       java
Requires:       python3

%description
The intelligent Python IDE with unique code assistance and analysis,
for productive Python development on all levels

%package doc
Summary:       Documentation for intelligent Python IDE
BuildArch:     noarch

%description doc
This package contains documentation for Intelligent Python IDE.

%prep
%autosetup


%build
CFLAGS="$RPM_OPT_FLAGS" %{__python3}
python3 helpers/pydev/setup_cython.py build_ext --inplace


%install
install -d %{buildroot}/%{_bindir}
install -d %{buildroot}/%{_datadir}/{applications,doc,pixmaps,pycharm}
install -d %{buildroot}%{_javadir}/%{name}
cp -arf ./{lib,bin,help,helpers,plugins} %{buildroot}%{_javadir}/%{name}/

rm -f %{buildroot}%{_javadir}/%{name}/bin/fsnotifier{,-arm}
# this will be in docs
rm -f %{buildroot}%{_javadir}/help/*.pdf

cp -af ./bin/pycharm.png %{buildroot}%{_datadir}/pixmaps/pycharm.png
cp -af %{SOURCE1} %{buildroot}%{_datadir}/applications/pycharm-community.desktop
ln -s %{_javadir}/%{name}/bin/pycharm.sh %{buildroot}%{_bindir}/pycharm


%files
%{_datadir}/applications/pycharm-community.desktop
%{_datadir}/pixmaps/pycharm.png
%{_javadir}/%{name}
%{_bindir}/pycharm

%files doc
%doc *.txt
%doc help/*.pdf
%license license/

%changelog
* Fri Dec 29 2017 Sebastian Zwierzchowski <sebastian.zwierzchowski@gmail.com> 2017.3.2
- Initial package
