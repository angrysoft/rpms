%global srcname yapf
%global sum A formatter for Python code.


Name:           python-%{srcname}
Version:        0.20.2
Release:        1%{?dist}
Summary:        %{sum}

License:        MIT
URL:            https://pypi.python.org/pypi/%{srcname}
Source0:        https://pypi.python.org/packages/c7/1c/d60905f2539110747cdfd5b13fe275f89b9be5f0b6bf3e589f200e5482e7/%{srcname}-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  python2-devel python3-devel

%description
A formatter for Python code.

%package -n python2-%{srcname}
Summary:        %{sum}
%{?python_provide:%python_provide python2-%{srcname}}

%description -n python2-%{srcname}
A formatter for Python code.


%package -n python3-%{srcname}
Summary:        %{sum}
%{?python_provide:%python_provide python3-%{srcname}}

%description -n python3-%{srcname}
A formatter for Python code.


%prep
%autosetup -n %{srcname}-%{version}

%build
%py2_build
%py3_build

%install
# Must do the python2 install first because the scripts in /usr/bin are
# overwritten with every setup.py install, and in general we want the
# python3 version to be the default.
# If, however, we're installing separate executables for python2 and python3,
# the order needs to be reversed so the unversioned executable is the python2 one.
%py2_install
%py3_install

%check
%{__python2} setup.py test
%{__python3} setup.py test

# Note that there is no %%files section for the unversioned python module if we are building for several python runtimes
%files -n python2-%{srcname}
#%license COPYING
%doc README.rst
%{python2_sitelib}/*

%files -n python3-%{srcname}
#%license COPYING
%doc README.rst
%{python3_sitelib}/*
%{_bindir}/yapf

%changelog
* Thu Mar 15 2018 Sebastian Zwierzchowski <sebastian.zwierzchowski@gmail.com> 0.20.2 
- Initial RPM release
