Name:           dart-sdk
Version:        2.4.0
Release:        2%{?dist}
Summary:        Dart SDK

License:        BSD
URL:            http://www.dartlang.org/
Source0:        https://storage.googleapis.com/dart-archive/channels/stable/release/%{version}/sdk/dartsdk-linux-x64-release.zip
      

%description
The dart programming language SDK

%prep
%autosetup -n dart-sdk

%install
install -d %{buildroot}/%{_bindir}
install -d %{buildroot}/%{_libdir}/dart/{bin,include,lib}
cp -a bin/* %{buildroot}/%{_libdir}/dart/bin
cp -a lib/* %{buildroot}/%{_libdir}/dart/lib
install include/* %{buildroot}/%{_libdir}/dart/include
install -m 644 version %{buildroot}/%{_libdir}/dart/version
ln -sf %{_libdir}/dart/bin/dart %{buildroot}/%{_bindir}/dart
ln -sf %{_libdir}/dart/bin/dart2aot %{buildroot}/%{_bindir}/dart2aot
ln -sf %{_libdir}/dart/bin/dart2js %{buildroot}/%{_bindir}/dart2js
ln -sf %{_libdir}/dart/bin/dartanalyzer %{buildroot}/%{_bindir}/dartanalyzer
ln -sf %{_libdir}/dart/bin/dartaotruntime %{buildroot}/%{_bindir}/dartaotruntime
ln -sf %{_libdir}/dart/bin/dartdevc %{buildroot}/%{_bindir}/dartdevc
ln -sf %{_libdir}/dart/bin/dartdoc %{buildroot}/%{_bindir}/dartdoc
ln -sf %{_libdir}/dart/bin/dartfmt %{buildroot}/%{_bindir}/dartfmt
ln -sf %{_libdir}/dart/bin/pub %{buildroot}/%{_bindir}/pub
find . -name "*~" -delete
find . -name "*.orig" -delete


%files
%license LICENSE
%doc README
%{_bindir}/*
%{_libdir}/*

%changelog
* Tue Jul 2 2019 Sebastian Zwierzchowski <sebastian.zwierzchowski@gmail.com> 2.4.0
- Version bump
* Sat May 11 2019 Sebastian Zwierzchowski <sebastian.zwierzchowski@gmail.com> 2.3.0
- add missing link to dart2aot, dartaotruntime.
* Thu May 9 2019 Sebastian Zwierzchowski <sebastian.zwierzchowski@gmail.com> 2.3.0
- Version bump
* Mon Mar 1 2019 Sebastian Zwierzchowski <sebastian.zwierzchowski@gmail.com> 2.2.0
- Version bump
* Thu Nov 29 2018 Sebastian Zwierzchowski <sebastian.zwierzchowski@gmail.com> 2.1.0
- Version bump
* Tue Aug 28 2018 Sebastian Zwierzchowski <sebastian.zwierzchowski@gmail.com> 2.0.0
- Version bump
* Thu Dec 28 2017 Sebastian Zwierzchowski <sebastian.zwierzchowski@gmail.com> 1.24.3
- initial package
