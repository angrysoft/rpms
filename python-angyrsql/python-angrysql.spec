%global srcname angrySQL

Name:           python3-%{srcname}
Version:        0.1
Release:        1%{?dist}
Summary:        angrySQL

License:        Apache-2.0
URL:            https://bitbucket.org/angrysoft/angrysql
Source0:        https://angrysoft.ovh/download/%{srcname}-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  python3-devel

%description
Simple Sql helper inspired by sqlalchemy but much simpler

%prep
%autosetup -n %{srcname}-%{version} 

%build
%py3_build

%install
%py3_install

%files -n python3-%{srcname}
#%license COPYING
%doc README.md
%{python3_sitelib}/*
#%{_bindir}/angrysql

%changelog
* Thu Mar 15 2018 Sebastian Zwierzchowski <sebastian.zwierzchowski@gmail.com> 0.1
- Initial RPM release
