%global srcname ledcolors

Name:           python3-%{srcname}
Version:        0.1
Release:        1%{?dist}
Summary:        LedColors

License:        Apache-2.0
URL:            https://bitbucket.org/angrysoft/ledcolors
Source0:        https://angrysoft.ovh/download/%{srcname}-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  python3-devel

%description
RGB / HSV led lamps colors helper.

%prep
%autosetup -n %{srcname}-%{version} 

%build
%py3_build

%install
%py3_install

%files -n python3-%{srcname}
#%doc README.md
%{python3_sitelib}/*

%changelog
* Mon Feb 18 2019 Sebastian Zwierzchowski <sebastian.zwierzchowski@gmail.com> 0.1
- Initial RPM release
